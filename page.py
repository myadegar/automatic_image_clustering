# coding=utf-8

import traceback
import sys

import scipy.spatial.distance as distance
import pytesseract

from utils.page_utils import *
from utils.join_box_horizontally import BoxConnector_horizontally
from utils.join_box_vertically import BoxConnector_vertically
from utils.utils import remove_lines


class Page(object):
    def __init__(self, im, page_num=0, lang=None, output_dir='temp'):
        self.healthy = True
        self.err = False
        self.page_num = page_num
        self.orig_im = im
        self.orig_im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        self.orig_shape = self.orig_im.shape
        self.lang = lang
        self.tess_config = '--psm 6'
        self.output_dir = output_dir

        # special setting for each document
        self.dilate_iteration = 3
        self.kernel_size = (3,3) # (7,5)
        self.intersect_thresh = 0.1
        self.down_scale = False
        self.reduce_noise = True
        self.save_regions = True
        self.remove_lines = True

        self.join_regions_horizontally = True
        self.max_horizontal_gap = 80
        self.min_vertical_overlaps = 0.5
        self.min_height_sim = 0.01

        self.join_regions_vertically = True
        self.max_vertical_gap = 40
        self.min_horizontal_overlaps = 0.8
        self.min_width_sim = 0.06

    def deskew(self):
        try:
            self.orig_im = deskew_image(self.orig_im)
            self.orig_im_gray = deskew_image(self.orig_im_gray)
            return self.orig_im_gray
        except Exception as e:
            self.err = e
            self.healthy = False

    def crop(self):
        try:
            self.segmented_rects , self.segmented_images, self.segmented_images_gray = self.process_image()
            return self.segmented_rects, self.segmented_images_gray
        except Exception as e:
            for frame in traceback.extract_tb(sys.exc_info()[2]):
                fname, lineno, fn, text = frame
                print("Error in %s on line %d" % (fname, lineno))
                print(e)
            self.err = e
            self.healthy = False

    def deskew_sub_image(self):
        try:
            self.segmented_images_gray, self.theta_est = process_skewed_crop(self.segmented_images_gray)
            for i, im in enumerate(self.segmented_images):
                cv2.imwrite(os.path.join(self.output_dir, 'segment_'+str(i+1)+'.jpg'), im)
            return self.segmented_images
        except Exception as e:
            self.err = e
            self.healthy = False

    def extract_text(self):
        self.texts = []
        for sub_image in self.segmented_images:
            text = pytesseract.image_to_string(sub_image, lang=self.lang, config=self.tess_config)
            self.texts.append(text)
        return self.texts

    def save(self, out_path):
        if not self.healthy:
            print("There was an error when cropping")
            raise Exception(self.err)
        else:
            for sub_image in self.segmented_images:
                cv2.imwrite(out_path, sub_image)

    def process_image(self):
        down_scale = self.down_scale
        reduce_noise = self.reduce_noise
        save_regions = self.save_regions
        kernel_size = self.kernel_size
        dilate_iteration = self.dilate_iteration
        intersect_thresh = self.intersect_thresh
        do_remove_lines = self.remove_lines

        # Load and scale down image.
        if down_scale:
            scale, im = downscale_image(self.orig_im_gray)
        else:
            scale = 1
            im = self.orig_im_gray.copy()

        # Reduce noise.
        if reduce_noise:
            im = reduce_noise_raw(im, median_size=3)
        h, w = im.shape[0], im.shape[1]

        # remove lines
        if do_remove_lines:
            im = remove_lines(im, horizontal=True, vertical=True, thick=3)

        # Edged.
        edges = auto_canny(im)


        # Reduce noise and remove thin borders.
        debordered = reduce_noise_edges(edges)

        # Dilate until there are a few components.
        # dilation, rects, num_tries = find_components(debordered, 16)
        dilation, rects, num_tries = find_all_components(debordered, kernel_size=kernel_size, dilate_iteration=dilate_iteration)

        # Find the final crop.
        # final_rect = find_final_crop(dilation, rects)

        # Find all crop regions.
        segmented_rects = find_segmented_rect(rects, thresh=intersect_thresh)

        # join near and simillar boxes horizontally
        if self.join_regions_horizontally:
            boxConn = BoxConnector_horizontally(MAX_HORIZONTAL_GAP = self.max_horizontal_gap,
                                                     MIN_V_OVERLAPS = self.min_vertical_overlaps,
                                                     MIN_SIZE_SIM = self.min_height_sim)
            segmented_rects = boxConn.get_text_lines(segmented_rects, [h, w], mode='max')
            segmented_rects = find_segmented_rect(segmented_rects, thresh=intersect_thresh)

        # join near and simillar boxes vertically
        if self.join_regions_vertically:
            boxConn = BoxConnector_vertically(MAX_VERTICAL_GAP = self.max_vertical_gap,
                                                     MIN_H_OVERLAPS = self.min_horizontal_overlaps,
                                                     MIN_SIZE_SIM = self.min_width_sim)
            segmented_rects = boxConn.get_text_lines(segmented_rects, [h, w])
            segmented_rects = find_segmented_rect(segmented_rects, thresh=intersect_thresh)

        # Sort rects from top to bottom and left to right
        segmented_rects = self.sort_rect(segmented_rects)

        # Extend box
        extended_segmented_rects = []
        for rect in segmented_rects:
            new_x1 = max(1, rect[0] - 5)
            new_y1 = max(1, rect[1]-5)
            new_x2 = min(w - 1, rect[2] + 5)
            new_y2 = min(h - 1, rect[3]+5)
            extended_segmented_rects.append((new_x1, new_y1, new_x2, new_y2))
        segmented_rects = extended_segmented_rects

        # Crop the image
        cropped_segmented_images = crop_image(self.orig_im, segmented_rects, scale)
        cropped_segmented_images_gray = crop_image(self.orig_im_gray, segmented_rects, scale)

        # smooth image
        # kernel = np.ones((5, 5), np.float32) / 25
        # smooth2d = cv2.filter2D(cropped[0], -1, kernel=kernel)

        if save_regions:
            im_regions = self.orig_im.copy()
            for i, rect in enumerate(segmented_rects):
                im_regions = cv2.rectangle(im_regions, rect[0:2], rect[2:], (0,0,255), 2)
                im_regions = cv2.putText(im_regions, str(i+1), (rect[0]-40,rect[1]+15), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0), 2)
            file_name = 'regions_' + str(self.page_num) + '.jpg'
            cv2.imwrite(os.path.join(self.output_dir, file_name), im_regions)

        return segmented_rects, cropped_segmented_images, cropped_segmented_images_gray


    def sort_rect(self, segmented_rects, threshold_value_y=40):
        rects = [[i, rect] for i, rect in enumerate(segmented_rects)]
        sorted_rects = sorted(rects, key=lambda x: x[1][1])
        num_rects = len(sorted_rects)
        # check if the next neighgour box x coordinates is greater then the current box x coordinates if not swap them.
        # repeat the swaping process to a threshold iteration and also select the threshold
        for i in range(5):
            for i in range(num_rects - 1):
                if abs(sorted_rects[i + 1][1][1] - sorted_rects[i][1][1]) < threshold_value_y and \
                        (sorted_rects[i + 1][1][0] < sorted_rects[i][1][0]):
                    tmp = sorted_rects[i]
                    sorted_rects[i] = sorted_rects[i + 1]
                    sorted_rects[i + 1] = tmp
        sorted_rects = [rect[1] for rect in sorted_rects]
        return sorted_rects
