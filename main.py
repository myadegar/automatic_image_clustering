from document import Document


def main(file_path):
    doc = Document(lang="fas+eng")
    doc.read(file_path)

    # Crop the pages down to estimated text regions, deskew, and optimize for OCR.
    doc.process()

    # Extract text from the pages.
    # doc.extract_text()
    # text = doc.get_text()

if __name__ == '__main__':
    # file_path = r"source\3.jpg"
    file_path = r"source\1.pdf"
    main(file_path)