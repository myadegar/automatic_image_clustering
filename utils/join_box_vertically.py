#-*- coding:utf-8 -*-
import numpy as np

class Graph:
    def __init__(self, graph):
        self.graph = graph

    def sub_graphs_connected(self):
        sub_graphs = []
        for index in range(self.graph.shape[0]):
            if not self.graph[:, index].any() and self.graph[index, :].any():
                v = index
                sub_graphs.append([v])
                while self.graph[v, :].any():
                    v = np.where(self.graph[v, :])[0][0]
                    sub_graphs[-1].append(v)
        return sub_graphs


class TextProposalGraphBuilder:
    """
        Build Text proposals into a graph.
    """
    def __init__(self, MAX_VERTICAL_GAP, MIN_H_OVERLAPS, MIN_SIZE_SIM):
        self.MAX_VERTICAL_GAP = MAX_VERTICAL_GAP
        self.MIN_H_OVERLAPS = MIN_H_OVERLAPS
        self.MIN_SIZE_SIM = MIN_SIZE_SIM

    def get_successions(self, index):
        box = self.text_proposals[index]
        results = []
        for left in range(int(box[1]) + 1, min(int(box[3]) + self.MAX_VERTICAL_GAP + 1, self.im_size[0])):
            adj_box_indices = self.boxes_table[left]
            for adj_box_index in adj_box_indices:
                if self.meet_h_iou(adj_box_index, index):
                    results.append(adj_box_index)
            if len(results) != 0:
                return results
        return results

    def meet_h_iou(self, index1, index2):
        def overlaps_h(index1, index2):
            w1 = self.widths[index1]
            w2 = self.widths[index2]
            x0 = max(self.text_proposals[index2][0], self.text_proposals[index1][0])
            x1 = min(self.text_proposals[index2][2], self.text_proposals[index1][2])
            return max(0, x1 - x0 + 1) / min(w1, w2)

        def size_similarity(index1, index2):
            w1 = self.widths[index1]
            w2 = self.widths[index2]
            return min(w1, w2) / max(w1, w2)

        return overlaps_h(index1, index2) >= self.MIN_H_OVERLAPS and \
               size_similarity(index1, index2) >= self.MIN_SIZE_SIM

    def build_graph(self, text_proposals, im_size):
        self.text_proposals = text_proposals
        self.im_size = im_size
        self.widths = text_proposals[:, 2] - text_proposals[:, 0] + 1

        boxes_table = [[] for _ in range(self.im_size[0])]
        for index, box in enumerate(text_proposals):
            boxes_table[int(box[1])].append(index)
        self.boxes_table = boxes_table

        graph = np.zeros((text_proposals.shape[0], text_proposals.shape[0]), np.bool)

        for index, box in enumerate(text_proposals):
            successions = self.get_successions(index)
            if len(successions) == 0:
                continue
            succession_index = successions[0]
            graph[index, succession_index] = True
        return Graph(graph)



class BoxConnector_vertically:
    """
        Connect text proposals into text lines
    """
    def __init__(self, MAX_VERTICAL_GAP=60, MIN_H_OVERLAPS=0.8, MIN_SIZE_SIM=0.8):
        self.graph_builder = TextProposalGraphBuilder(MAX_VERTICAL_GAP, MIN_H_OVERLAPS, MIN_SIZE_SIM)

    def group_text_proposals(self, text_proposals, im_size):
        graph = self.graph_builder.build_graph(text_proposals, im_size)
        return graph.sub_graphs_connected()


    def get_text_lines(self, text_proposals, im_size):
        """
        text_proposals:boxes

        """
        # tp=text proposal
        text_proposals = np.array(text_proposals)
        tp_groups = self.group_text_proposals(text_proposals, im_size)

        box_indices = list(range(text_proposals.shape[0]))
        temp = []
        [temp.extend(gr) for gr in tp_groups]
        joined_box_indices = set(temp)
        non_joined_box_indices = [ind for ind in box_indices if ind not in joined_box_indices]

        text_lines = np.zeros((len(tp_groups), 4), np.float32)

        for index, tp_indices in enumerate(tp_groups):
            text_line_boxes = text_proposals[list(tp_indices)]
            x0 = np.min(text_line_boxes[:, 0])
            y0 = np.min(text_line_boxes[:, 1])
            x1 = np.max(text_line_boxes[:, 2])
            y1 = np.max(text_line_boxes[:, 3])
            text_lines[index, 0] = x0
            text_lines[index, 1] = y0
            text_lines[index, 2] = x1
            text_lines[index, 3] = y1

        text_recs = [tuple(text_proposals[ind]) for ind in non_joined_box_indices]
        for line in text_lines:
            text_recs.append((int(line[0]), int(line[1]), int(line[2]), int(line[3])))

        return text_recs
