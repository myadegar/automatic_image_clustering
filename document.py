# coding=utf-8

import os
import mimetypes
import PyPDF2 as pyPdf
from pdf2image import convert_from_path
import cv2
import glob

from page import Page


acceptable_mime = ["image/bmp", "image/png", "image/tiff", "image/jpeg",
                   "image/jpg", "video/JPEG", "video/jpeg2000"]


FileNotAcceptedException = Exception(
    'The filetype is not acceptable. We accept bmp, png, tiff, jpg, jpeg, jpeg2000, and PDF.'
)


class Document(object):
    def __init__(self, lang=None):
        self.lang = lang
        self.pages = []
        self.processed_pages = []
        self.page_content = []
        self.prepared = False
        self.error = None
        self.deskew_image = False
        self.deskew_sub_image = True
        self.output_dir = 'output'
        ##
        os.makedirs(self.output_dir, exist_ok=True)
        for file in glob.glob(self.output_dir + '*/*'):
            os.remove(file)

    def read(self, path):
        self.filename = os.path.basename(path)
        self.file_basename, self.file_extension = os.path.splitext(self.filename)
        self.path = path
        self.mime_type = mimetypes.guess_type(path)
        self.file_basepath = os.path.dirname(path)

        # If the file is a pdf, split the pdf and prep the pages.
        if self.mime_type[0] == "application/pdf":
            file_temp = open(self.path, 'rb')
            pdf_reader = pyPdf.PdfFileReader(file_temp)
            self.num_pages = pdf_reader.numPages
            try:
                for i in range(self.num_pages):
                    output = pyPdf.PdfFileWriter()
                    output.addPage(pdf_reader.getPage(i))
                    path = 'temp.pdf'
                    im_path = 'temp.png'
                    with open(path, 'wb') as f:
                        output.write(f)
                    page = convert_from_path(pdf_path=path, dpi=200, last_page=1)[0]
                    page.save(im_path, 'JPEG')
                    orig_im = cv2.imread(im_path)
                    page = Page(im=orig_im, page_num=i, lang=self.lang, output_dir=self.output_dir)
                    self.pages.append(page)
                    os.remove(path)
                    os.remove(im_path)
                self.prepared = True
            except Exception as e:
                self.error = e
                raise

        # If the file is an image, think of it as a 1-page pdf.
        elif self.mime_type[0] in acceptable_mime:
            self.num_pages = 1
            orig_im = cv2.imread(path)
            page = Page(im=orig_im, page_num=0, lang=self.lang, output_dir=self.output_dir)
            self.pages.append(page)

        # Otherwise, out of luck.
        else:
            print(self.mime_type[0])
            raise FileNotAcceptedException

    def process(self):
        for page in self.pages:
            new = page
            if self.deskew_image:
                new.deskew()
            new.crop()
            if self.deskew_sub_image:
                new.deskew_sub_image()
            self.processed_pages.append(new)

    def extract_text(self):
        if len(self.processed_pages) > 0:
            for page in self.processed_pages:
                new = page
                texts = new.extract_text()
                self.page_content.append(texts)
        else:
            raise Exception('You must run `process()` first.')

    def get_text(self):
        if len(self.page_content) > 0:
            text_pages = []
            for i, page_content in enumerate(self.page_content):
                file_name = 'page_' + str(i) + '.txt'
                text_page = ''
                for j, text in enumerate(page_content):
                    text = text.strip()
                    text_page += 'Block ' + str(j+1) + ':' + '\n\n'
                    text_page += (text + '\n\n')
                    text_page += ('-'*100 + '\n')
                text_pages.append(text_page)
                with open(os.path.join(self.output_dir, file_name), mode='w', encoding='utf-8') as f:
                    f.write(text_page)
            return text_pages
        else:
            raise Exception('You must run `extract_text()` first.')

    def save_pages(self):
        # TODO
        pass
